; PPC Makefile
; ppc.make

core = 7.x
api = 2


; PPC Core Feature
projects[ppc_core][download][type] = git
projects[ppc_core][download][url] = git@bitbucket.org:mattyohe/ppc_core.git
projects[ppc_core][type] = "module"

; PPC Forkenbrock Feature
projects[ppc_forkenbrock][download][type] = git
projects[ppc_forkenbrock][download][url] = git@bitbucket.org:mattyohe/ppc_forkenbrock.git
projects[ppc_forkenbrock][type] = "module"

; PPC General Feature
projects[ppc_general][download][type] = git
projects[ppc_general][download][url] = git@bitbucket.org:mattyohe/ppc_general.git
projects[ppc_general][type] = "module"

; PPC Program Pages Feature
projects[ppc_program_pages][download][type] = git
projects[ppc_program_pages][download][url] = git@bitbucket.org:mattyohe/ppc_program_pages.git
projects[ppc_program_pages][type] = "module"

; PPC Users Feature
projects[ppc_users][download][type] = git
projects[ppc_users][download][url] = git@bitbucket.org:mattyohe/ppc_users.git
projects[ppc_users][type] = "module"

; PPC Users Feature
projects[ppc_front_page][download][type] = git
projects[ppc_front_page][download][url] = git@bitbucket.org:mattyohe/ppc_front_page.git
projects[ppc_front_page][type] = "module"

; PPC Theme
projects[ppc][type] = theme
projects[ppc][download][type] = git
projects[ppc][download][url] = git@bitbucket.org:rory_o/ppc_theme.git

libraries[profiler][download][type] = "get"
libraries[profiler][download][url] = "http://ftp.drupal.org/files/projects/profiler-7.x-2.0-beta1.tar.gz"



; Contrib Modules
projects[advanced_help][version] = "1.0-beta1"

projects[apachesolr][version] = "1.0-beta8"

projects[biblio][version] = "1.x-dev"

projects[block_class][version] = "1.x-dev"

projects[ctools][version] = "1.0-rc1"

projects[ckeditor_link][version] = "2.1"

projects[collapse_text][version] = "2.3"

projects[context][version] = "3.0-beta2"

projects[date][version] = "2.x-dev"

projects[delta][version] = "3.0-beta8"

projects[diff][version] = "2.0"

projects[entity][version] = "1.0-beta10"

projects[features_extra][version] = "1.x-dev"

projects[features][version] = "1.0-beta4"

projects[feeds][version] = "2.0-alpha4"

projects[file_entity][version] = "2.0-unstable2"

projects[styles][version] = "2.0-alpha8"

projects[imagecrop][version] = "1.0-rc3"

projects[job_scheduler][version] = "2.0-alpha2"

projects[ldap][version] = "1.0-beta4"

projects[media][version] = "2.0-unstable2"

projects[media_youtube][version] = "1.0-alpha5"

projects[menu_position][version] = "1.0-beta2"

projects[module_filter][version] = "1.5"

projects[references][version] = "2.0-beta3"

projects[nodequeue][version] = "2.x-dev"

projects[pathauto][version] = "1.0-rc2"

projects[quicktabs][version] = "3.0"

projects[rb][version] = "1.x-dev"

projects[realname][version] = "1.0-rc2"

projects[services][version] = "3.0"

projects[rules][version] = "2.0-rc2"

projects[taxonomy_csv][version] = "5.6"

projects[taxonomy_manager][version] = "1.0-beta2"

projects[term_reference_tree][version] = "1.6"

projects[token][version] = "1.0-beta5"

projects[views][version] = "3.0-rc1"

projects[wysiwyg][version] = "2.1"

projects[omega][version] = "3.0"

; If we need to use a patch
; projects[imagecache][patch][] = http://drupal.org/files/.....pathto.patch

