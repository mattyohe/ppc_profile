<?php

!function_exists('profiler_v2') ? require_once('libraries/profiler/profiler.inc') : FALSE;


profiler_v2('ppc_test');

/**
 * Implement hook_install().
 *
 * Perform actions to set up the site for this profile.
 */

function ppc_test_install() {
  /* Do nothing here */

}

/**
 * Implements hook_install_tasks().
 */


function ppc_test_install_tasks($install_state) {

  include_once('libraries/profiler/profiler_api.inc');

  return array(
      'ppc_tax' => array(
        'display_name' => st('Set up taxonomies'),
        'display' => TRUE,
        'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
        'type' => 'batch',
        'function' => 'ppc_test_update_tax'
        ),
      'profiler_install_profile_complete' => array(),
      );
}

function ppc_test_update_tax(){

  $xml;
  $vocab_path = DRUPAL_ROOT . '/profiles/ppc_test/vocab.xml';

  if (file_exists($vocab_path)) {
    $xml = simplexml_load_file($vocab_path);

  } else {
    exit('Failed to open ' . $vocab_path);
  }

  $vid = 'vid';
  $name = 'name';


  // Get every taxonomy_vocabulary row as $node
  foreach($xml->children()->children() as $node){

    $cur_vid = $node->$vid;
    $cur_name = $node->$name;

    $result = db_update('taxonomy_vocabulary')
      ->fields(array('vid' => $cur_vid))
      ->condition('name', $cur_name)
      ->execute();


  }

  $tid = 'tid';
  $description = 'description';
  $format = 'format';
  $weight = 'weight';

  $term_path = DRUPAL_ROOT . '/profiles/ppc_test/terms.xml';

  if (file_exists($term_path)) {
    $xml = simplexml_load_file($term_path);

  } else {
    exit('Failed to open ' . $term_path);
  }

  foreach($xml->children()->children() as $node){

    $cur_tid = $node->$tid;
    $cur_vid = $node->$vid;
    $cur_name = $node->$name;
    $cur_description = $node->$description;
    $cur_format = $node->$format;
    $cur_weight = $node->$weight;


    db_insert('taxonomy_term_data')
      ->fields(array(
        'tid' => $cur_tid,
        'vid' => $cur_vid,
        'name' => $cur_name,
        'description' => $cur_description,
        'format' => $cur_format,
        'weight' => $cur_weight
      ))
      ->execute();


  }

  $hierarchy_path = DRUPAL_ROOT . '/profiles/ppc_test/term_hierarchy.xml';

  if (file_exists($hierarchy_path)) {
    $xml = simplexml_load_file($hierarchy_path);

  } else {
    exit('Failed to open ' . $hierarchy_path);
  }

  $parent = 'parent';

  foreach($xml->children()->children() as $node){

    $cur_tid = $node->$tid;
    $cur_parent = $node->$parent;

    db_insert('taxonomy_term_hierarchy')
      ->fields(array(
        'tid' => $cur_tid,
        'parent' => $cur_parent
      ))
      ->execute();


  }




}



