; RENAME THIS FILE TO ppcsite.make
; drush make ppcsite.make newsitefoldername
; cd newsitefoldername
; drush site-install ppc_test --db-url=sqlite://db.sqlite


core = 7.x
api = 2


projects[drupal][version] = "7.8"
projects[ppc_test][type] = profile
projects[ppc_test][download][type] = get
; Change the URL below to a local zipped file containing ppc_test.info ppc_test.make ppc_test.profile
projects[ppc_test][download][url] = "https://bitbucket.org/mattyohe/ppc_profile/get/master.zip"

